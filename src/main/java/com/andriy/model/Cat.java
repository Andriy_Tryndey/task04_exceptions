package com.andriy.model;

public class Cat extends Animal {
    private int id;

    /**
     * defoult constructor
     */
    public Cat() {
    }

    /**
     * constructor with parameters
     * @param kind
     * @param klikuha
     * @param id
     */
    public Cat(String kind, String klikuha, int id) {
        super(kind, klikuha);
        this.id = id;
    }

    /**
     * what animal conversation
     */
    @Override
    public void conversation() {
        System.out.println("may-may");
    }

    public int getId() {
        return id;
    }

    public Cat setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id= " + id + ", " + super.toString() +
                '}';
    }
}
