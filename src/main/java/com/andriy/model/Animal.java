package com.andriy.model;

/**
 * parent class
 */
public class Animal {
    private String kind;
    private String klikuha;

    /**
     * default constructor
     */
    public Animal() {
    }

    /**
     * constuctor with param
     * @param kind
     * @param klikuha
     */
    public Animal(String kind, String klikuha) {
        this.kind = kind;
        this.klikuha = klikuha;
    }

    public void conversation() {
        System.out.println("rrrrrrrrrrrrr");
    }

    public String getKind() {
        return kind;
    }

    public Animal setKind(String kind) {
        this.kind = kind;
        return this;
    }

    public String getKlikuha() {
        return klikuha;
    }

    public Animal setKlikuha(String klikuha) {
        this.klikuha = klikuha;
        return this;
    }

    @Override
    public String toString() {
        return "Animal{" +
                ", kind='" + kind + '\'' +
                ", klikuha='" + klikuha + '\'' +
                '}';
    }
}
