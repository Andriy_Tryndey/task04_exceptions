package com.andriy.model;

public class Dog extends Animal {
    private int id;

    /**
     * defoult constructor
     */
    public Dog() {
    }

    /**
     * constructor with parameters
     * @param kind
     * @param klikuha
     * @param id
     */
    public Dog(String kind, String klikuha, int id) {
        super(kind, klikuha);
        this.id = id;
    }

    /**
     * what animal conversation
     */
    @Override
    public void conversation() {
        System.out.println("gav - gav");
    }

    public int getId() {
        return id;
    }

    public Dog setId(int id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "id = " + id + ", " + super.toString() +
                '}';
    }
}
