package com.andriy.model;

import java.util.ArrayList;
import java.util.List;

public class ZooClab {

    private int amountDog;
    private int amountCat;
    private List<Dog> dogList;
    private List<Cat> catList;

    /**
     * constructor create zooclab object
     */
    public ZooClab() {
        dogList = new ArrayList<>();
        catList = new ArrayList<>();
        fillingDogList();
        fillingCatList();
    }

    /**
     * fill doglist permanent members clad
     */
    public void fillingDogList(){
        dogList.add(new Dog("dog", "bursik", 1));
        dogList.add(new Dog("dog", "sharik", 2));
    }

    /**
     * fill catlist permanent members clad
     */
    public void fillingCatList(){
        catList.add(new Cat("cat", "mashka", 1));
        catList.add(new Cat( "cat", "moorka", 2));
    }

    public int getAmountDog() {
        return amountDog;
    }

    public ZooClab setAmountDog(int amountDog) {
        this.amountDog = amountDog;
        return this;
    }

    public int getAmountCat() {
        return amountCat;
    }

    public ZooClab setAmountCat(int amountCat) {
        this.amountCat = amountCat;
        return this;
    }

    public List<Dog> getDogList() {
        return dogList;
    }

    public ZooClab setDogList(List<Dog> dogList) {
        this.dogList = dogList;
        return this;
    }

    public List<Cat> getCatList() {
        return catList;
    }

    public ZooClab setCatList(List<Cat> catList) {
        this.catList = catList;
        return this;
    }
}
