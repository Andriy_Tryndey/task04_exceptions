package com.andriy.controller;

import com.andriy.model.*;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    /**
     * create model businessLogic
     */
    public ControllerImpl(){
        model = new BusinessLogic();
    }

    /**
     * add animal in to the base
     * @param animal
     */
    @Override
    public void addAnimal(Animal animal) {
        if (animal.getKind().equals("dog")){
            model.addDogToTheDogs(animal);
        }else if(animal.getKind().equals("cat")){
            model.addCatToTheCats(animal);
        }
    }

    /**
     * find all dogs in the database
     * @return
     */
    @Override
    public List<Dog> findAllDog() {
        return model.findAllDogs();
    }

    /**
     * find all cats in the database
     * @return
     */
    @Override
    public List<Cat> findAllCat() {
        return model.findAllCat();
    }

    /**
     * find animal by number and kind in the database
     * @param id
     * @param kind
     * @return
     */
    @Override
    public Animal findAnimalByIdAndKind(int id, String kind) {
        return model.findAnimalByIdKind(id, kind);
    }
}
