package com.andriy.view;

import com.andriy.MyParameterExeption;

@FunctionalInterface
public interface Printable {
    void print() throws MyParameterExeption;
}
