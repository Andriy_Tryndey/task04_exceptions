package com.andriy.controller;

import com.andriy.model.Animal;
import com.andriy.model.Cat;
import com.andriy.model.Dog;

import java.util.List;

public interface Controller {
    void addAnimal(Animal animal);
    List<Dog> findAllDog();
    List<Cat> findAllCat();
    Animal findAnimalByIdAndKind(int id, String kind);
}
