package com.andriy.model;

import java.util.List;

public interface Model {

    void addDogToTheDogs(Animal animal);
    void addCatToTheCats(Animal animal);
    List<Dog> findAllDogs();
    List<Cat> findAllCat();
    Animal findAnimalByIdKind(int id, String kind);
}
