package com.andriy.view;

import com.andriy.MyParameterExeption;
import com.andriy.controller.ControllerImpl;
import com.andriy.model.Animal;
import com.andriy.model.Cat;
import com.andriy.model.Dog;

import java.util.*;

/**
 * create menu
 */
public class MyView {
    private ControllerImpl controller;
    private static Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView(){
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Input dog in the zooclab");
        menu.put("2", " 2 - Input cat in the zooclab");
        menu.put("3", " 3 - look at dogs");
        menu.put("4", " 4 - look at cats");
        menu.put("5", " 5 - delete the animals");
        menu.put("Q", " Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    /**
     * reaction to button1 press
     */
    private void pressButton1() throws MyParameterExeption {
        Animal animal = inputAnimal();
        System.out.println(controller.findAllDog());
        System.out.println("Input number : ");
        int number = 0;
        try {
            number = sc.nextInt();
            Animal animalD = new Dog(animal.getKind(), animal.getKlikuha(), number);
            controller.addAnimal(animalD);
            System.out.println(controller.findAllDog());
        }catch (InputMismatchException ex){
            ex.printStackTrace();
            System.out.println("Entered value is not an integer");
        }
    }

    /**
     * reaction to button2 press
     */
    private void pressButton2() throws MyParameterExeption {
        Animal animal = inputAnimal();
        System.out.println(controller.findAllCat());
        System.out.println("Input number : ");
        int number = 0;
        try {
            number = sc.nextInt();
            Animal animalC = new Cat(animal.getKind(), animal.getKlikuha(), number);
            controller.addAnimal(animalC);
            System.out.println(controller.findAllCat());
        }catch (InputMismatchException ex){
            ex.printStackTrace();
            System.out.println("Entered value is not an integer");
        }
    }

    /**
     * reaction to button3 press
     */
    private void pressButton3() {
        System.out.println(controller.findAllDog());
    }

    /**
     * reaction to button4 press
     */
    private void pressButton4() {
        System.out.println(controller.findAllCat());
    }

    /**
     * reaction to button5 press
     */
    private void pressButton5() {
        List<Dog> allDog = controller.findAllDog();
        System.out.println(allDog);
            System.out.println("input kind");
            String kind = sc.nextLine().toLowerCase();
            System.out.println("input number");
            int number = sc.nextInt();
            Dog d = new Dog();
            for (Dog dog : allDog) {
                if (kind.equals(dog.getKind()) && number == dog.getId()){
                    d = dog;
                }
            }
            System.out.println(d);
            allDog.remove(d);

        List<Cat> allCat = controller.findAllCat();
        Cat c = new Cat();
        for (Cat cat : allCat) {
            if (kind.equals(cat.getKind()) && number == cat.getId()){
                c = cat;
            }
        }
        allCat.remove(c);
        System.out.println(allCat);
    }

    private void outputMenu(){
        System.out.println("\nMENU");
        for (String str : menu.values()){
            System.out.println(str);
        }
    }

    /**
     * method of selecting a menu item
     */
    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e) {

            }
        }while (!keyMenu.equals("Q"));
    }

    /**
     * parameter input method
     * @return
     */
    public Animal inputAnimal()throws MyParameterExeption{
        Animal animal = null;
        System.out.println("Input new animal : ");
        System.out.println("Input kind : ");
        String kind = sc.nextLine().toLowerCase();
        System.out.println("Input klikuha : ");
        String klikuha = sc.nextLine();
        try {
            if (animal.getKind() != null && animal.getKlikuha() != null){
                animal = new Animal(kind, klikuha);
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();
            System.out.println("one or more fields are missing to create the object");
        }

        return animal;
    }


}
