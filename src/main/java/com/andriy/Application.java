package com.andriy;

import com.andriy.view.MyView;

/**
 * main application startup class
 */
public class Application {
    /**
     * entry point to the program
     * @param args
     */
    public static void main(String[] args) {
        new MyView().show();
    }
}
