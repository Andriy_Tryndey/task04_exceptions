package com.andriy.model;

import java.util.List;

/**
 * program execution logic
 */
public class BusinessLogic implements Model {
    private ZooClab zooClab;

    /**
     * create zooclab object
     */
    public BusinessLogic(){
        zooClab = new ZooClab();
    }

    /**
     * add a dog to the dog list
     * @param animal
     */
    @Override
    public void addDogToTheDogs(Animal animal) {
        zooClab.getDogList().add((Dog) animal);
    }

    /**
     * add a cat to the dog list
     * @param animal
     */
    @Override
    public void addCatToTheCats(Animal animal) {
        zooClab.getCatList().add((Cat) animal);
    }

    /**
     * bring out all dogs
     * @return
     */
    @Override
    public List<Dog> findAllDogs() {
        return zooClab.getDogList();
    }

    /**
     * bring out all cats
     * @return
     */
    @Override
    public List<Cat> findAllCat() {
        return zooClab.getCatList();
    }

    /**
     * find dog or cat by numbe and kind
     * @param id
     * @param kind
     * @return
     */
    @Override
    public Animal findAnimalByIdKind(int id, String kind) {
        Animal animal = null;
        if (kind == "dog"){
            for (Dog d : zooClab.getDogList()) {
                if (id == d.getId()) {
                    animal = d;
                }
            }
        }
        if (kind == "cat"){
            for (Cat c : zooClab.getCatList()) {
                if (id == c.getId()){
                    animal = c;
                }
            }
        }
        return animal;
    }


}
